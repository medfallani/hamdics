using UnityEngine;

public class joserobinson : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static joserobinson USE;

    private AudioSource addiemullen;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            addiemullen = transform.GetChild(0).GetComponent<AudioSource>();

            corahart();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void corahart()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void andreale(AudioClip clip)
    {
        addiemullen.PlayOneShot(clip);
    }
}
