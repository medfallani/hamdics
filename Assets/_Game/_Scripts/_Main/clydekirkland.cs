using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class clydekirkland : MonoBehaviour
{
    public Camera cameraObj;
    public arturolin coloringMenu, paintingMenu;

    [System.Serializable]
    public class arturolin
    {
        public GameObject leighseymour;
        public Color color;
        public Image image;
        public Sprite mayrahunt;
        public Sprite jillkinney;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.leighseymour.SetActive(isPainting);
        coloringMenu.leighseymour.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.mayrahunt : paintingMenu.jillkinney;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.mayrahunt : coloringMenu.jillkinney;
    }

    public void concettapereira()
    {

    }

    public void claricekey()
    {
        if (salvadorstinson.Instance.francinewoodward)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
