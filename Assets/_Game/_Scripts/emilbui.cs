using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class emilbui : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool faycarrasco = false;
    [System.Serializable]
    public class robbiepearce : UnityEvent { }
    [SerializeField]
    private robbiepearce myOwnEvent = new robbiepearce();
    public robbiepearce onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, marilynnolan = 1f;
    private Vector3 startPosition, katelyncrenshaw;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (faycarrasco)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (faycarrasco)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
    }

    private IEnumerator latonyakeller()
    {
        yield return estherstallings(transform, transform.localPosition, katelyncrenshaw, marilynnolan);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float jasminewall = 1.0f / value;
        float ameliamccauley = 0.0f;
        while (ameliamccauley < 1.0)
        {
            ameliamccauley += Time.deltaTime * jasminewall;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, ameliamccauley));
            yield return null;
        }

        thisTransform.localPosition = katelyncrenshaw;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        katelyncrenshaw = EPos;

        marilynnolan = MTime;

        StartCoroutine(latonyakeller());
    }
}
