using Google.Play.Review;
using Google.Play.Core.Internal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reviewmyapp : MonoBehaviour
{
    // Start is called before the first frame update
    private ReviewManager _reviewManager; 
      PlayReviewInfo _playReviewInfo;

    void Start()
    {
        _reviewManager = new ReviewManager();

        StartCoroutine(review());

    }


  
    // Update is called once per frame
    void Update()
    {
     }

    public IEnumerator review()
    {
        yield return new WaitForSeconds(1f);

        var requestFlowOperation = _reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            yield break;
        }
        _playReviewInfo = requestFlowOperation.GetResult();

        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null;
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            yield break;
        }
    }

}
