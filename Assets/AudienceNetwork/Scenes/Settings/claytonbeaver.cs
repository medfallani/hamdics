using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class claytonbeaver : MonoBehaviour
{
    private static string faypena = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string silviamackey;

    
    public static void lindsaymetz()
    {
        string prefix = PlayerPrefs.GetString(faypena, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        silviamackey = PlayerPrefs.GetString(faypena, "");
        urlPrefixInput.text = silviamackey;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        silviamackey = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(faypena, silviamackey);
        AudienceNetwork.AdSettings.SetUrlPrefix(silviamackey);
    }

    public void AdViewScene()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void InterstitialAdScene()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void RewardedVideoAdScene()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
